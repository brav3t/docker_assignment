# Docker assignment part 1

## 0. dockerfile and startup.bat is rewritten and added to gitlab repository.
## 1. Build docker image:
![1. Build docker image](/DOCKER_PART1/images/1_docker_build_and_verify.png)
## 2. Create container:
![2. Create container](/DOCKER_PART1/images/2_docker_create_container.png)
## 3. Write out log:
```
docker logs test_container
```
![3. Write out log](/DOCKER_PART1/images/3_docker_write_out_log.png)
## 4. Log file on host:
![4. Log file on host](/DOCKER_PART1/images/4_docker_log_file_on_host.png)
## 5. Log file contents:
![5. Log file contents](/DOCKER_PART1/images/5_docker_log_file_contents.png)
## 6. Push image to repository:
![6. Push image to repository](/DOCKER_PART1/images/6_docker_push.png)
## 7. Image on repository:
![7. Image on repository](/DOCKER_PART1/images/7_image_on_repo.png)
