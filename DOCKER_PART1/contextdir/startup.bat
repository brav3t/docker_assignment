echo "Write out container's root folder's contents to a time stamped logfile that is can be accessed from host."
set d=%DATE:~-4%-%DATE:~4,2%-%DATE:~7,2%
set t=%time::=.% 
set t=%t: =%
set logfile="%d%_%t%.log"
dir > c:\test_results\log_%logfile%.txt
