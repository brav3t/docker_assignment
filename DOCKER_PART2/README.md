# Docker assignment part 2

## 0. dockerfile, requirements.txt, test_nothing.py are created.
## 1. Build docker image:
![1. Build docker image](/DOCKER_PART2/images/1_docker_build_py_test.png)
## 2. Run image:
![2. Run image](/DOCKER_PART2/images/2_docker_run_image.png)
## 3. Run tests:
![3. Run tests](/DOCKER_PART2/images/3_docker_run_tests.png)
